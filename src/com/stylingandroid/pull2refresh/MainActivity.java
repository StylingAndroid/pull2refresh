package com.stylingandroid.pull2refresh;

import java.util.Arrays;

import com.handmark.pulltorefresh.library.PullToRefreshListView;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

public class MainActivity extends Activity {
	private static final String[] sContent = new String[] { 
		"One", "Two", "Three", "Four", "Five", "Six", 
		"Seven", "Eight", "Nine", "Ten", "Eleven", 
		"Twelve", "Thirteen", "Fourteen", "Fifteen", 
		"Sixteen", "Seventeen", "Eighteen", "Nineteen", 
		"Twenty" };
	private ArrayAdapter<String> mAdapter;
	private MenuItem mRefresh = null;
	private Handler mHandler = new Handler();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1,
				Arrays.asList(sContent));
		PullToRefreshListView listView = (PullToRefreshListView)findViewById(android.R.id.list);
		listView.setAdapter(mAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		mRefresh = menu.findItem(R.id.menu_refresh);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_refresh) {
			item.setActionView(R.layout.layout_progress);
			mHandler.postDelayed(new Runnable() {

				@Override
				public void run() {
					refreshComplete();
				}
			}, 2000);
		}
		return super.onOptionsItemSelected(item);
	}

	private void refreshComplete() {
		mRefresh.setActionView(null);
	}

}
